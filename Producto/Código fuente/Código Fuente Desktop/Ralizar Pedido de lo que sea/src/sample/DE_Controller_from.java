package sample;

import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;


public class DE_Controller_from implements Initializable {


    //private String texto = "";
    Alert dialogoAlerta = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private Button siguiente;
    @FXML
    private Button btnBuscarImagen;
    @FXML
    private Button contYGuardar;

    @FXML
    private ImageView iviImage;
    @FXML
    private ImageView imagenDetalle;


    @FXML
    private AnchorPane helpPanel;
    @FXML
    private AnchorPane pedidoPanel;
    @FXML
    private AnchorPane infoPedidoPanel;
    @FXML
    private AnchorPane imagenPanel;
    @FXML
    private AnchorPane creditCardPanel;
    @FXML
    private AnchorPane efectivoPanel;
    @FXML
    private AnchorPane detallePanel;
    @FXML
    private AnchorPane confirmacionPanel;
    @FXML
    private AnchorPane graciasPanel;
    @FXML
    private AnchorPane detallePagoEfectivoPanel;
    @FXML
    private AnchorPane detallePagoTarjetaPanel;


    @FXML
    private JFXTextArea textPedido;
    @FXML
    private JFXTextArea textPedidoFinal;
    @FXML
    private JFXTextArea direccionOrigen;
    @FXML
    private JFXTextArea direccionDestino;
    @FXML
    private JFXTextArea textPedidoFinal1;
    @FXML
    private JFXTextArea direccionOrigen1;
    @FXML
    private JFXTextArea direccionDestino1;

    @FXML
    private TextField fechaVenc;
    @FXML
    private TextField nombreTitular;
    @FXML
    private TextField nroTarjeta;
    @FXML
    private TextField codSeguridad;
    @FXML
    private TextField cantEfective;
    @FXML
    private TextField vuelto;
    @FXML
    private TextField precioProducto;

    @FXML
    private TextField fechaVenc1;
    @FXML
    private TextField nombreTitular1;
    @FXML
    private TextField nroTarjeta1;
    @FXML
    private TextField codSeguridad1;
    @FXML
    private TextField cantEfective1;
    @FXML
    private TextField vuelto1;
    @FXML
    private TextField precioProducto1;
    @FXML
    private JFXTextField metodoDePago1;

    @FXML
    private ComboBox<String> comboMetodoPago;

    @FXML
    private RadioButton cantidadEfectivo;
    @FXML
    private RadioButton noLoSe;
    @FXML
    private RadioButton cantidadEfectivo1;
    @FXML
    private RadioButton noLoSe1;

    @FXML
    private JFXToggleButton antesPosible;
    @FXML
    private JFXToggleButton antesPosible1;

    @FXML
    private JFXTimePicker horaEntrega;
    @FXML
    private JFXDatePicker fechaEntrega;
    @FXML
    private JFXTimePicker horaEntrega1;
    @FXML
    private JFXDatePicker fechaEntrega1;


    ObservableList<String> comboId =
            FXCollections.observableArrayList("Pago en efectivo", "Pago con tarjeta");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pedidoPanel.setVisible(true);
        infoPedidoPanel.setVisible(false);
        creditCardPanel.setVisible(false);
        efectivoPanel.setVisible(false);
        helpPanel.setVisible(false);
        comboMetodoPago.setItems(comboId);
        ToggleGroup grupo = new ToggleGroup();
        noLoSe.setToggleGroup(grupo);
        cantidadEfectivo.setToggleGroup(grupo);
        cantEfective.setText("0");
        verificarTextField(codSeguridad);
        verificarTextField(nroTarjeta);
        verificarTextField(precioProducto);
        verificarTextField(cantEfective);

    }


    public void showDate() {
        Date d = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("MM/YY");
        fechaVenc.setText(sf.format(d));
    }
    //Metodo para salir de la aplicacion
    public void salirAplicacion(MouseEvent event) {
        confirmarSalida("¿Realmente quiere salir?");


    }
    //Metodo para continuar al detalle del pedido
    public void continuarDetallePedido(MouseEvent event) {
        String texto = textPedido.getText();

        if (texto.length() > 2) { //Valida que se ingrese algun pedido
          //  siguiente.setDisable(true);
            infoPedidoPanel.setVisible(true);
            pedidoPanel.setVisible(false);
            textPedidoFinal.setText(textPedido.getText());
        } else mensajesDeError("Error", "Debe ingresar un pedido");
    }
    //Metodo para entrar al panel que carga imagenes
    public void buscarImagen(MouseEvent event) {
        imagenPanel.setVisible(true);

    }
    //Metodo para buscar y setear imagenes
    public void setImagen() {

        btnBuscarImagen.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");

            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );

            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(null);

            // Mostar la imagen
            if (imgFile != null) {
                Image image = new Image("file:" + imgFile.getAbsolutePath());
                iviImage.setImage(image);
            }
        });

    }
    //Metodo para confirmar y volver al panel anterior
    public void clickHecho(MouseEvent event) {
        imagenPanel.setVisible(false);
    }
    //Metodo para ingresar al panel de ayuda
    public void clickHelp(MouseEvent event) {
        helpPanel.setVisible(true);
    }
    //Metodo para volver al panel de pedido
    public void clickEntendido(MouseEvent event) {
        helpPanel.setVisible(false);
        pedidoPanel.setVisible(true);
    }
    //Metodo para confirmar el pedido, tmb valida que todos los campos esten ingresado
    public void clickConfirmar(MouseEvent event) {
        if (textPedidoFinal.getLength() > 2 & direccionOrigen.getLength() > 2
                & direccionDestino.getLength() > 2 & horaEntrega.getEditor().getLength() > 2
                & fechaEntrega.getEditor().getLength() > 2 & precioProducto.getLength() > 2 & comboMetodoPago.getSelectionModel().getSelectedIndex() > -1) {

            confirmarPedido();
            if (comboMetodoPago.getSelectionModel().getSelectedIndex() == 0) {
                detallePagoEfectivoPanel.setVisible(true);
                detallePagoTarjetaPanel.setVisible(false);
                detallePanel.setVisible(true);
            }else   if (comboMetodoPago.getSelectionModel().getSelectedItem() == "Pago con tarjeta") {
                detallePagoEfectivoPanel.setVisible(false);
                detallePagoTarjetaPanel.setVisible(true);
                detallePanel.setVisible(true);
            }

        } else if (textPedidoFinal.getLength() > 2 & direccionOrigen.getLength() > 2
                & direccionDestino.getLength() > 2 & antesPosible.isSelected() == true & precioProducto.getLength() > 2 & comboMetodoPago.getSelectionModel().getSelectedIndex() > -1) {
            confirmarPedido();
            if (comboMetodoPago.getSelectionModel().getSelectedIndex() == 0) {
                detallePagoEfectivoPanel.setVisible(true);
                detallePagoTarjetaPanel.setVisible(false);
                detallePanel.setVisible(true);
            }else   if (comboMetodoPago.getSelectionModel().getSelectedItem() == "Pago con tarjeta") {
                detallePagoEfectivoPanel.setVisible(false);
                detallePagoTarjetaPanel.setVisible(true);
                detallePanel.setVisible(true);
            }

        } else {

            mensajesDeError("Error", "Asegurese de llenar todos los campos.");
        }
    }

    //Metodo para salir del efectivo sin guardar
    public void atrasEfectivo(MouseEvent event) {
       confirmarSiguiente("¿Seguro de volver atras?",efectivoPanel,infoPedidoPanel);

      //  efectivoPanel.setVisible(false);
        if (efectivoPanel.isVisible()==false) {
            comboMetodoPago.setValue(null);
            vuelto.clear();
            cantEfective.clear();
            noLoSe.setSelected(false);
            cantidadEfectivo.setSelected(false);
        }

    }

    public void atrasTarjeta(MouseEvent event) {
        confirmarSiguiente("¿Seguro de volver atras?",creditCardPanel,infoPedidoPanel);
        // creditCardPanel.setVisible(false);
        if (creditCardPanel.isVisible()== false) {

            comboMetodoPago.getSelectionModel().clearSelection();
            nombreTitular.clear();
            nroTarjeta.clear();
            codSeguridad.clear();
            fechaVenc.clear();
        }

    }

    public void atrasPedido(MouseEvent event) {
        pedidoPanel.setVisible(true);
        infoPedidoPanel.setVisible(false);

    }

    @FXML//Metodo para seleccionar elmetodo de pago y ingresar al respectivo panel (Tarjeta o efectivo)
    public void ingresarMetodoPago(MouseEvent event) {

        String tarjeta = "Pago con tarjeta";
        String efectivo = "Pago en efectivo";
        if (precioProducto.getLength() == 0) {
            mensajesDeError("Error","Ingrese un precio aproximado");
            comboMetodoPago.setValue(null);

            precioProducto.requestFocus();
        } else {comboMetodoPago.setOnAction(event1 -> {
            String comboValue = comboMetodoPago.getSelectionModel().getSelectedItem();
            if (precioProducto.getLength() > 1) {
                if (comboValue.equals(efectivo)) {
                    efectivoPanel.setVisible(true);
                    cantEfective.setDisable(true);
                }
                if (comboValue.equals(tarjeta)) creditCardPanel.setVisible(true);
            }
        });
          /**  String comboValue = comboMetodoPago.getSelectionModel().getSelectedItem();
            if (precioProducto.getLength() > 1) {
                if (comboValue.equals(efectivo)) {
                    efectivoPanel.setVisible(true);
                    cantEfective.setDisable(true);
                }
                if (comboValue.equals(tarjeta)) creditCardPanel.setVisible(true);
            }*/
        }/**else if (precioProducto.getLength() == 0) { //Valida que haya ingresado el precio del producto
            mensajesDeError("Error","Ingrese un precio aproximado");
            comboMetodoPago.setValue(null);

            precioProducto.requestFocus();
        }*/
    }


    public void continuarTarjeta(MouseEvent event) throws IOException {
        int MIN_NOMBRE_TITULAR = 5;
        int CANT_DIGITOS_TARJETA = 16;
        int MIN_COD_SEGURIDAD = 3;
        int MAX_COD_SEGURIDAD=4;
        System.out.println(String.valueOf(fechaVenc.getLength()));
        if (nombreTitular.getLength() < MIN_NOMBRE_TITULAR){
            mensajesDeError("Error","El nombre del titular esta mal");
            nombreTitular.requestFocus();
        }else if (nroTarjeta.getLength()!=CANT_DIGITOS_TARJETA){
            mensajesDeError("Error","El número de tarjeta esta mal");
            nroTarjeta.requestFocus();
        }else if (fechaVenc.getLength() != 5 ){
            mensajesDeError("Error","La fecha de vencimiento esta mal");
            fechaVenc.requestFocus();
        }else if (codSeguridad.getLength() != MIN_COD_SEGURIDAD & codSeguridad.getLength() != MAX_COD_SEGURIDAD){
            mensajesDeError("Error","El código de seguridad esta mal");
            codSeguridad.requestFocus();
        }else {
            confirmarSiguiente("¿Esta seguro que los datos ingresados son correctos?",creditCardPanel,pedidoPanel);
          //  creditCardPanel.setVisible(false);
            //pedidoPanel.setVisible(true);
        }


    }

    public void continuarEfectivoClick(MouseEvent event) {
        int PRECIO_DELIVERY = 40;
        int auxTotal = Integer.parseInt(precioProducto.getText()) + PRECIO_DELIVERY;
        if (noLoSe.isSelected() == true) {
            pedidoPanel.setVisible(true);
            efectivoPanel.setVisible(false);
        }

        if (cantidadEfectivo.isSelected() == true & auxTotal <= Integer.parseInt(cantEfective.getText())) {
            if (vuelto.getLength() == 0){
                int auxVuelto = Integer.parseInt(cantEfective.getText()) - (PRECIO_DELIVERY + Integer.parseInt(precioProducto.getText()));
                vuelto.setText(String.valueOf(auxVuelto));
            }else {
                    pedidoPanel.setVisible(true);
                    efectivoPanel.setVisible(false);
                    }
        } else if (cantidadEfectivo.isSelected() == true & Integer.parseInt(cantEfective.getText()) < auxTotal) {
            mensajesDeError("Error", "El monto debe ser superior a: $" + auxTotal);
        } else
        if ((cantidadEfectivo.isSelected() == false) & noLoSe.isSelected() == false) {
            mensajesDeError("Error", "Asegurese de seleccionar una opción.");
        }
    }

    //Metodo que habilita el textfield cantidad de efectivo
    public void habilitarCantidadEfectivo(MouseEvent event) {
        if (cantidadEfectivo.isSelected() == true) {
            cantEfective.setDisable(false);
        }
    }

    @FXML//Metodo para calcular el vuelto
    public void calcularVuelto(MouseEvent event) {
        int PRECIO_DELIVERY = 40;
        if (cantEfective.getLength()>2) {
            int aux = Integer.parseInt(cantEfective.getText()) - (PRECIO_DELIVERY + Integer.parseInt(precioProducto.getText()));
            vuelto.setText(String.valueOf(aux));
        }

    }
    //Metodo que selecciona la opcion "No lo se" y limpia los demas textfield
    public void seleccionarNoLoSe(MouseEvent event) {
        if (noLoSe.isSelected() == true) {
            cantEfective.clear();
            vuelto.clear();
            cantEfective.setDisable(true);
        }

    }
    //Metodo que habilita la opcion lo antes posible, y bloquea fecha y hora de entrega
    public void seleccionarLoAntesPosible(MouseEvent event) {
        if (antesPosible.isSelected() == false) {
            fechaEntrega.setDisable(false);
            horaEntrega.setDisable(false);

        }
        if (antesPosible.isSelected() == true) {
            horaEntrega.getEditor().setText("");
            fechaEntrega.getEditor().setText("");
            fechaEntrega.setDisable(true);
            horaEntrega.setDisable(true);
        }
    }

    public void mensajesDeError(String titulo, String contenido) {
        dialogoAlerta.setTitle(titulo);
        dialogoAlerta.setHeaderText(null);
        dialogoAlerta.setContentText(contenido);
        dialogoAlerta.showAndWait();

    }


    private void verificarTextField(TextField textField) {
        textField.textProperty().addListener(agregarListenerAlTextField(textField));
    }
    //Metodo que asegura que solo se ingrese digitos
    private ChangeListener<String> agregarListenerAlTextField(TextField aTextField) {
        return (observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")/**&!newValue.matches("'\\b' ")*/){
                aTextField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        };
    }

    //
    public void confirmarPedido() {
        imagenDetalle.setImage(iviImage.getImage());
        textPedidoFinal1.setText(textPedidoFinal.getText());
        direccionOrigen1.setText(direccionOrigen.getText());
        direccionDestino1.setText(direccionDestino.getText());
        fechaVenc1.setText(fechaVenc.getText());
        nombreTitular1.setText(nombreTitular.getText());
        nroTarjeta1.setText(nroTarjeta.getText());
        codSeguridad1.setText(codSeguridad.getText());
        cantEfective1.setText(cantEfective.getText());
        vuelto1.setText(vuelto.getText());
        precioProducto1.setText(precioProducto.getText());
        metodoDePago1.setText(comboMetodoPago.getValue());
        if (cantidadEfectivo.isSelected() == true) {
            cantidadEfectivo1.setSelected(true);
        }
        if (noLoSe.isSelected() == true) {
            noLoSe1.setSelected(true);
        }
        if (antesPosible.isSelected() == true) {
            antesPosible1.setSelected(true);
        } else if   (antesPosible.isSelected()==false) {

            horaEntrega1.setValue(horaEntrega.getValue());
            fechaEntrega1.setValue(fechaEntrega.getValue());

    }
    }
    public void confirmarSalida(String contenido) {
        Alert dialogo = new Alert(Alert.AlertType.CONFIRMATION);
        dialogo.setTitle("Confirmación");
        dialogo.setHeaderText(null);
        dialogo.setContentText(contenido);
        Optional<ButtonType>result=dialogo.showAndWait();
        if (result.get() == ButtonType.OK){
            Platform.exit();
            System.exit(0);
        }
    }

    public  void confirmarSiguiente(String contenido,AnchorPane origen, AnchorPane destino){
        Alert dialogo = new Alert(Alert.AlertType.CONFIRMATION);
        dialogo.setTitle("Confirmación");
        dialogo.setHeaderText(null);
        dialogo.setContentText(contenido);
        Optional<ButtonType>result=dialogo.showAndWait();
        if (result.get() == ButtonType.OK){
            destino.setVisible(true);
            origen.setVisible(false);
        }
    }


    public void imprimirPedido() {
        contYGuardar.setOnAction(event -> {
            TextInputDialog dialog = new TextInputDialog("G:\\");
            dialog.setTitle("Guardar tu pedido");

            dialog.setContentText("Por favor, ingrese una ruta");
            Optional<String> result = dialog.showAndWait();

            WritableImage image = confirmacionPanel.snapshot(new SnapshotParameters(), null);
            int aux1 = (new Date().getHours());
            int aux2 = new Date().getMinutes();
            int aux3 = new Date().getSeconds();
            String date = String.valueOf(aux1);
            date = date + String.valueOf(aux2);
            date = date + String.valueOf(aux3);

            System.out.println(date);

            File file = new File(result.get() +  date + ".png");

            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            graciasPanel.setVisible(true);
            detallePanel.setVisible(false);
        });
    }
    public void atrasDetalle(){
        confirmarSiguiente("Esta seguro de volver atras",detallePanel,infoPedidoPanel);
    }
}
